import { VideoEditor, CreateThumbnailOptions } from "@ionic-native/video-editor";
import { StreamingMedia, StreamingVideoOptions } from "@ionic-native/streaming-media";
import { MediaCapture, MediaFile, CaptureVideoOptions, CaptureError } from "@ionic-native/media-capture";
import { Media, MediaObject } from "@ionic-native/media";
import { Component, ViewChild } from "@angular/core";
import { NavController } from "ionic-angular";
import { Storage } from "@ionic/storage";
import { File } from "@ionic-native/file";

const MEDIA_FILES_KEY = "mediaFiles";

@Component({
  selector: "page-home",
  templateUrl: "home.html"
})
export class HomePage {
  @ViewChild("myvideo")
  myVideo: any;
  mediaFiles = [];
  fromDirectory: any;
  thumbImg: any;
  images: Array<{ src: String }>;
  fileNameWeh: any;

  constructor(
    public navCtrl: NavController,
    private mediaCapture: MediaCapture,
    private storage: Storage,
    private media: Media,
    private file: File,
    private streamingMedia: StreamingMedia,
    private videoEditor: VideoEditor,
  ) {}

  ionViewDidLoad() {
    this.storage.get(MEDIA_FILES_KEY).then(res => {
      this.mediaFiles = JSON.parse(res) || [];
    });
  }

  public captureAudio() {
    this.mediaCapture.captureAudio().then(res => {
      this.storeMediaFiles(res);
    });
  }

  public captureVideo() {
    let options: CaptureVideoOptions = {
      limit: 1,
      duration: 30
    };

    this.mediaCapture.captureVideo(options).then(
      (res: MediaFile[]) => {
        let capturedFile = res[0];
        let fileName = capturedFile.name;
        this.fileNameWeh = fileName;
        let dir = capturedFile["localURL"].split("/");
        dir.pop();
        this.fromDirectory = dir.join("/");
        var toDirectory = this.file.dataDirectory;

        this.file.copyFile(this.fromDirectory, fileName, toDirectory, fileName).then(
          res => {
            this.storeMediaFiles([
              {
                name: fileName,
                size: capturedFile.size,
                loc: toDirectory,
              }
            ]);
          },
          err => {
            console.log("err: ", err);
          }
        ).then(() => {
          var nItem = localStorage.getItem('videoNum');
          var numstr = 0;

          if(nItem == null){
            numstr = 1;
          }else{
            numstr = parseInt(nItem,10);
            numstr = numstr + 1;
          }

          var option:CreateThumbnailOptions = {
            fileUri: this.file.dataDirectory + this.fileNameWeh,
            width:160,
            height:206,
            atTime:1,
            outputFileName:'sample' + numstr,
            quality:100
          };

          this.videoEditor.createThumbnail(option).then(result => {
            localStorage.setItem('videoNum', numstr.toString());

            var currentName = result.substr(result.lastIndexOf('/') + 1);
            var correctPath = result.substr(0, result.lastIndexOf('/') + 1);

            this.file.readAsDataURL('file://' + correctPath, currentName).then(ruru => {
              this.thumbImg = ruru;
              this.images.unshift({
                src: 'data:image/jpeg;base64,' + ruru
              });
            });

          }).catch(e => {
            alert(e);
          });
        });
      },
      (err: CaptureError) => console.error(err)
    )
  }

  public play(myFile) {
    if (myFile.name.indexOf(".wav") > -1) {
      const audioFile: MediaObject = this.media.create(myFile.localURL);
      audioFile.play();
    } else {
      let fileName = myFile.name;
      let path = this.file.dataDirectory + fileName;
      let options: StreamingVideoOptions = {
        successCallback: () => {
          console.log("Video played");
        },
        errorCallback: e => {
          console.log("Error streaming");
        },
        orientation: "potrait",
        shouldAutoClose: false,
        controls: true
      };

      this.streamingMedia.playVideo(path, options);
    }
  }

  public storeMediaFiles(files) {
    this.storage.get(MEDIA_FILES_KEY).then(res => {
      if (res) {
        let arr = JSON.parse(res);
        arr = arr.concat(files);
        this.storage.set(MEDIA_FILES_KEY, JSON.stringify(arr));
      } else {
        this.storage.set(MEDIA_FILES_KEY, JSON.stringify(files));
      }
      this.mediaFiles = this.mediaFiles.concat(files);
    });
  }

}
